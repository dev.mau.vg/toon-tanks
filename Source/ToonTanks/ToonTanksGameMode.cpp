#include "ToonTanksGameMode.h"

#include "PlayerControllerBase.h"
#include "Kismet/GameplayStatics.h"
#include "ToonTanks/PawnTank.h"
#include "ToonTanks/PawnTurret.h"

void AToonTanksGameMode::BeginPlay()
{
	Super::BeginPlay();

	HandleGameStart();
}

void AToonTanksGameMode::HandleGameStart()
{

	TargetTurrets = GetTargetTurretsCount();

	PlayerTank = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));
	PC = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));

	GameStart();
	
	if (PC != nullptr)
	{
		PC->SetPlayerEnabledState(false);

		FTimerHandle PlayerEnableHandle;
		const FTimerDelegate PlayerEnableDelegate = FTimerDelegate::CreateUObject(PC, &APlayerControllerBase::SetPlayerEnabledState, true);

		GetWorld()->GetTimerManager().SetTimer(PlayerEnableHandle, PlayerEnableDelegate, StartDelay, false);
	}
}

void AToonTanksGameMode::HandleGameOver(bool PlayerWon)
{
	GameOver(PlayerWon);
}

int32 AToonTanksGameMode::GetTargetTurretsCount() const
{
	TArray<AActor*> TurretActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APawnTurret::StaticClass(), TurretActors);

	return TurretActors.Num();
}

void AToonTanksGameMode::ActorDied(AActor* DeadActor)
{
	if (DeadActor == PlayerTank)
	{
		PlayerTank->HandleDestruction();
		HandleGameOver(false);

		if (PC != nullptr)
		{
			PC->SetPlayerEnabledState(false);
		}
	}
	else if (APawnTurret* ActorDying = Cast<APawnTurret>(DeadActor))
	{
		ActorDying->HandleDestruction();

		TargetTurrets--;

		if (TargetTurrets == 0)
		{
			HandleGameOver(true);
		}
	}
}
